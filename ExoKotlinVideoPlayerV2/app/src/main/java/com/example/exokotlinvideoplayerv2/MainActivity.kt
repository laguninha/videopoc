package com.example.exokotlinvideoplayerv2

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

const val EXTRA_URL = "com.example.exokotlinvideoplayerv2.EXTRA_URL"

class MainActivity : AppCompatActivity() {

    private val mediaUrl = "https://storage.googleapis.com/exoplayer-test-media-0/BigBuckBunny_320x180.mp4"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button_teste.setOnClickListener {
            val intent = Intent(this, PlayerActivity::class.java)
            intent.putExtra(EXTRA_URL, mediaUrl)
            startActivity(intent)
        }
    }

}
