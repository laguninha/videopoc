package com.example.exokotlinvideoplayerv2

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.ExoPlayer.EventListener
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_player.*
import kotlinx.android.synthetic.main.exo_playback_control_view.*


class PlayerActivity : AppCompatActivity() {

    private var player: SimpleExoPlayer? = null
    private var playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition: Long = 0

    private val videoUrl: String by lazy {
        intent?.extras?.getString(EXTRA_URL) as String
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
        playbackButtonsListeners()
    }

    override fun onResume() {
        super.onResume()
        hideSystemUi()
        if (Util.SDK_INT < 24) initPlayer()
    }

    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT >= 24) initPlayer()
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT < 24) releasePlayer()
    }

    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT >= 24) releasePlayer()
    }

    private fun initPlayer() {
        if (player == null) {
            player = ExoPlayerFactory.newSimpleInstance(this, getTrackSelector())
        }
        player_view.player = player
        buildSingleMediaSource(videoUrl)
        player?.playWhenReady = playWhenReady
        player?.seekTo(currentWindow, playbackPosition)
        player?.addListener(playbackStateListener())
    }

    private fun getTrackSelector(): TrackSelector {
        val trackSelector = DefaultTrackSelector()
        trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoSizeSd())

        return trackSelector
    }

    private fun buildSingleMediaSource(videoUrl: String) {
        val uri = Uri.parse(videoUrl)
        val videoSource = ExtractorMediaSource
            .Factory(getDataSourceFactory())
            .createMediaSource(uri)
        player?.prepare(videoSource)
    }

    private fun getDataSourceFactory(): DefaultDataSourceFactory {
        return DefaultDataSourceFactory(
            this,
            Util.getUserAgent(this, getString(R.string.app_name))
        )
    }

    private fun releasePlayer() {
        player?.let { player ->
            playWhenReady = player.playWhenReady
            playbackPosition = player.currentPosition
            currentWindow = player.currentWindowIndex
            //player.removeListener(playbackStateListener)
            player.release()
            this.player = null
        }
    }

    private fun playbackButtonsListeners() {
        image_back_button.setOnClickListener {
            onBackPressed()
        }
        image_pause_button.setOnClickListener {
            player?.playWhenReady = false
            image_play_button.visibility = View.VISIBLE
            image_pause_button.visibility = View.GONE
        }
        image_play_button.setOnClickListener {
            player?.playWhenReady = true
            image_pause_button.visibility = View.VISIBLE
            image_play_button.visibility = View.GONE
        }
    }

    @SuppressLint("InlinedApi")
    private fun hideSystemUi() {
        player_view.systemUiVisibility = (View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }

    private fun playbackStateListener(): Player.EventListener {
        return object: EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    ExoPlayer.STATE_BUFFERING -> println("ninja state: Buffering")
                    ExoPlayer.STATE_IDLE -> println("ninja state: Idle")
                    ExoPlayer.STATE_READY -> {
                        println("ninja state: Ready")
                        if (playWhenReady) {
                            image_play_button.visibility = View.GONE
                            image_pause_button.visibility = View.VISIBLE
                            loader.visibility = View.GONE
                        } else {
                            image_play_button.visibility = View.VISIBLE
                            image_pause_button.visibility = View.GONE
                            loader.visibility = View.GONE
                        }



                    }
                    ExoPlayer.STATE_ENDED -> println("ninja state: Ended")
                }
            }
            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
            }

            override fun onSeekProcessed() {
            }

            override fun onTracksChanged(
                trackGroups: TrackGroupArray?,
                trackSelections: TrackSelectionArray?
            ) {
            }

            override fun onPlayerError(error: ExoPlaybackException?) {
            }

            override fun onLoadingChanged(isLoading: Boolean) {
            }

            override fun onPositionDiscontinuity(reason: Int) {
            }

            override fun onRepeatModeChanged(repeatMode: Int) {
            }

            override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
            }

            override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {}
        }
    }

    private fun fullScreenClickEvent() {
//        exo_fullscreen_icon.setOnClickListener {
//            if (fullscreen) {
//                exo_fullscreen_icon.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        this@MainActivity,
//                        R.drawable.exo_controls_fullscreen_enter
//                    )
//                )
//                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
//                if (supportActionBar != null) {
//                    supportActionBar?.show()
//                }
//                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
//                val params =
//                    player_view.layoutParams
//                params.width = ViewGroup.LayoutParams.MATCH_PARENT
//                params.height = ViewGroup.LayoutParams.WRAP_CONTENT
//                player_view.layoutParams = params
//                fullscreen = false
//            } else {
//                exo_fullscreen_icon.setImageDrawable(
//                    ContextCompat.getDrawable(
//                        this@MainActivity,
//                        R.drawable.exo_controls_fullscreen_exit
//                    )
//                )
//                window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_FULLSCREEN
//                        or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
//                if (supportActionBar != null) {
//                    supportActionBar?.hide()
//                }
//                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
//                val params =
//                    player_view.layoutParams
//                params.width = ViewGroup.LayoutParams.MATCH_PARENT
//                params.height = ViewGroup.LayoutParams.MATCH_PARENT
//                player_view.layoutParams = params
//                fullscreen = true
//            }
//        }
    }
}

